package cmnetty;

import org.apache.camel.Handler;

import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 * User: ion
 * Date: 9/27/12
 * Time: 3:40 PM
 * Sample message.
 */
public class TimerBean {
    @Handler
    public String generateMessage(){
        return "The current time is " + Calendar.getInstance().getTime().toString();
    }
}
