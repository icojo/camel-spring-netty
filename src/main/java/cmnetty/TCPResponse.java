package cmnetty;

import org.apache.camel.Handler;

/**
 * Created with IntelliJ IDEA.
 * User: ion
 * Date: 9/27/12
 * Time: 5:48 PM
 * Simple echo and append message handler.
 */
public class TCPResponse {
    @Handler
    public String generateMessage(String message){
        return message + " confirmed by TCP backend.";
    }
}
