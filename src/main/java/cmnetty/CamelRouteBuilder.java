package cmnetty;

import org.apache.camel.ExchangePattern;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.spring.Main;

/**
 * A Camel Router
 */
public class CamelRouteBuilder extends RouteBuilder {

    public static void main(String... args) throws Exception {
        Main.main(args);
    }

    public void configure() {

        from("netty:tcp://localhost:5150").bean(TCPResponse.class).setExchangePattern(ExchangePattern.InOut);
        // expose a echo websocket server, that sends back an echo
        from("websocket://echo")
                .log(">>> Message received from WebSocket Client : ${body}")
                .transform().simple("time is ${date:now:ss:mm}")
                //forward message to the TCP back end
                .to("netty:tcp://localhost:5150")
                //send back to the client TCP response
                .to("websocket://echo");
        //timer to send a message every 50 ms to a random ws client
        from("timer://message_producer?period=50").beanRef("messageTimer")
                .to("seda:wsClient?concurrentConsumers=10&size=10&blockWhenFull=true");
        //send it to the server
        from("seda:wsClient?concurrentConsumers=10&size=10&blockWhenFull=true")
                .beanRef("asyncHttpClient");
    }
}
