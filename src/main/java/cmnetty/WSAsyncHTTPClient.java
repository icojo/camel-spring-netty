package cmnetty;

import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.websocket.WebSocket;
import com.ning.http.client.websocket.WebSocketTextListener;
import com.ning.http.client.websocket.WebSocketUpgradeHandler;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: ion
 * Date: 9/27/12
 * Time: 4:20 PM
 * A Simple Web Socket Client
 */
public class WSAsyncHTTPClient {
    private String uri;

    public WSAsyncHTTPClient(String uri) {

        this.uri = uri;
    }

    public void sendTextMessage(String message) {
        //TODO reuse weboscket connections. was not the point of the exercise
        createWebSocket(message);

    }

    private WebSocket createWebSocket(String message) {
        WebSocket socket = null;
        AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        CountDownLatch latch = new CountDownLatch(1);
        try {
            socket = asyncHttpClient.prepareGet(uri).execute(
                    new WebSocketUpgradeHandler.Builder()
                            .addWebSocketListener(new WebSocketTextListener() {
                                @Override
                                public void onMessage(String message) {

                                    System.out.println("<<< received from server " + message);

                                }

                                @Override
                                public void onFragment(String fragment, boolean last) {
                                }

                                @Override
                                public void onOpen(WebSocket websocket) {
                                }

                                @Override
                                public void onClose(WebSocket websocket) {
                                }

                                @Override
                                public void onError(Throwable t) {
                                    t.printStackTrace();
                                }
                            }).build()).get();
            socket.sendTextMessage(message);
            latch.await(1, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        socket.close();
        asyncHttpClient.close();
        return socket;
    }
}
